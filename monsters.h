#pragma once

#include <string>
#include <vector>
#include "dice.h"

class monsters {
	public:
		monsters();
		std::string name = "";
		std::string desc = "";
		std::vector<int> color;
		std::vector<int> abil;
		dice* speed;

		dice* hitpoints;
		dice* damage;
		char symbol= ' ';
		int rarity = -1;

		/** code from piazza to make parsing colors more efficient**/
		const std::string colors[8] = {
					  "BLACK",
					  "RED",
					  "GREEN",
					  "YELLOW",
					  "BLUE",
					  "MAGENTA",
					  "CYAN",
					  "WHITE",
		};

		const std::string abils[9] = {
			  "SMART",
			  "TELE",
			  "TUNNEL",
			  "ERRATIC",
			  "PASS",
			  "PICKUP",
			  "DESTROY",
			  "UNIQ",
			  "BOSS"
		};



		void setName(std::string nameStr);
		void setDesc(std::string description);
		void setColor(std::string colorStr);
		void setAbils(std::string abilsStr);
		void setSpeed(std::string speedStr);
		void setHP(std::string hpStr);
		void setDam(std::string damStr);


};


