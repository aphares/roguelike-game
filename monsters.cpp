#include <stdlib.h>     /* atoi */
#include <iostream>
#include <algorithm>

#include "monsters.h"


monsters::monsters() {

}

void monsters::setName(std::string nameStr) {
	name = nameStr;
}

void monsters::setDesc(std::string description) {
	desc = description;
}

void monsters::setColor(std::string colorStr) {

	std::string tmp = colorStr;
	int size = 0;

	std::string tmpstr(tmp.begin(), std::find(tmp.begin(), tmp.end(), ' '));
	for (int i = 0; i < 8; i++) {
		if (colors[i].compare(tmpstr) == 0) {
			color.push_back(i);
		}
	}
	size += tmpstr.size() + 1;


	while ((int) colorStr.size() > size) {
		tmp = colorStr.substr(size);
		std::string tmpstr(tmp.begin(), std::find(tmp.begin(), tmp.end(), ' '));
		for (int i = 0; i < 8; i++) {
			if (colors[i].compare(tmpstr) == 0) {
				color.push_back(i);
			}
		}
		size += tmpstr.size() + 1;
	}
}

void monsters::setAbils(std::string abilsStr) {

	std::string tmp = abilsStr;
	int size = 0;

	std::string tmpstr(tmp.begin(), std::find(tmp.begin(), tmp.end(), ' '));
	for (int i = 0; i < 8; i++) {
		if (abils[i].compare(tmpstr) == 0) {
			abil.push_back(i);
		}
	}
	size += tmpstr.size() + 1;


	while ((int)abilsStr.size() > size) {
		tmp = abilsStr.substr(size);
		std::string tmpstr(tmp.begin(), std::find(tmp.begin(), tmp.end(), ' '));
		for (int i = 0; i < 8; i++) {
			if (abils[i].compare(tmpstr) == 0) {
				abil.push_back(i);
			}
		}
		size += tmpstr.size() + 1;
	}
}

void monsters::setSpeed(std::string speedStr) {
	
	//Sample: 9+3d8
	// set b to 9
	std::string b(speedStr.begin(), std::find(speedStr.begin(), speedStr.end(), '+'));
	// set tmp to 3d8
	std::string tmp = speedStr.substr(b.size()+1);
	//set d to 3
	std::string d(tmp.begin(), std::find(tmp.begin(), tmp.end(), 'd'));
	// set s to 8
	std::string s = speedStr.substr(b.size()+d.size()+2);

	speed = new dice(&b,&d,&s);


}

void monsters::setHP(std::string hpStr) {

	//Sample: 9+3d8
	// set b to 9
	std::string b(hpStr.begin(), std::find(hpStr.begin(), hpStr.end(), '+'));
	// set tmp to 3d8
	std::string tmp = hpStr.substr(b.size() + 1);
	//set d to 3
	std::string d(tmp.begin(), std::find(tmp.begin(), tmp.end(), 'd'));
	// set s to 8
	std::string s = hpStr.substr(b.size() + d.size() + 2);

	hitpoints = new dice(&b, &d, &s);


}

void monsters::setDam(std::string damStr) {

	//Sample: 9+3d8
	// set b to 9
	std::string b(damStr.begin(), std::find(damStr.begin(), damStr.end(), '+'));
	// set tmp to 3d8
	std::string tmp = damStr.substr(b.size() + 1);
	//set d to 3
	std::string d(tmp.begin(), std::find(tmp.begin(), tmp.end(), 'd'));
	// set s to 8
	std::string s = damStr.substr(b.size() + d.size() + 2);

	damage = new dice(&b, &d, &s);


}
