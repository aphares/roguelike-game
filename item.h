#include <string>

class item {
public:
	char symbol;
	pair_t position;
	uint32_t color;
	bool isArtifact;
	std::string name;
	std::string description;
	dice damage;
	int32_t hit;
	int32_t dodge;
	int32_t defence;
	int32_t weight;
	int32_t speed;
	int32_t attribute;
	int32_t value;

};

void gen_items(dungeon *d);
void destroy_items(dungeon *d);