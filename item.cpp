#include <stdlib.h>
#include <string.h>

#include "utils.h"
#include "npc.h"
#include "dungeon.h"
#include "move.h"
#include "path.h"
#include "event.h"
#include "pc.h"
#include "descriptions.h"



/*const std::string object_symbol[19] = {
	"WEAPON",
	"OFFHAND",
	"RANGED",
	"LIGHT",
	"ARMOR",
	"HELMET",
	"CLOAK",
	"GLOVES",
	"BOOTS",
	"AMULET",
	"RING",
	"SCROLL",
	"BOOK",
	"FLASK",
	"GOLD",
	"AMMUNITION",
	"FOOD",
	"WAND",
	"CONTAINER"
}; */

extern const char object_symbols[] = {
  '*', /* objtype_no_type */
  '|', /* objtype_WEAPON */
  ')', /* objtype_OFFHAND */
  '}', /* objtype_RANGED */
  '~', /* objtype_LIGHT */
  '[', /* objtype_ARMOR */
  ']', /* objtype_HELMET */
  '(', /* objtype_CLOAK */
  '{', /* objtype_GLOVES */
  '\\', /* objtype_BOOTS */
  '"', /* objtype_AMULET */
  '=', /* objtype_RING */
  '`', /* objtype_SCROLL */
  '?', /* objtype_BOOK */
  '!', /* objtype_FLASK */
  '$', /* objtype_GOLD */
  '/', /* objtype_AMMUNITION */
  ',', /* objtype_FOOD */
  '-', /* objtype_WAND */
  '%', /* objtype_CONTAINER */
};


void gen_items(dungeon *d)
{
	object_description object;
	item *m;
	uint32_t room, vectorSize, rarity;
	pair_t p;
	uint32_t i;


	for (i = 0; i < d->object_descriptions.size(); i++) {
		d->artifacts.push_back(1);
	}
	uint32_t num_items = rand_range(10, 25);
	for (i = 0; i < num_items; i++) {
		while (true) {
			vectorSize = rand_range(0, d->object_descriptions.size() - 1);
			rarity = rand_range(0, 99);
			if (d->artifacts.at(vectorSize) != 0 && rarity >= d->object_descriptions.at(vectorSize).rarity) {
				object = d->object_descriptions.at(vectorSize);
				break;
			}
		}

		m = new item;

		do {
			room = rand_range(1, d->num_rooms - 1);
			p[dim_y] = rand_range(d->rooms[room].position[dim_y],
				(d->rooms[room].position[dim_y] +
					d->rooms[room].size[dim_y] - 1));
			p[dim_x] = rand_range(d->rooms[room].position[dim_x],
				(d->rooms[room].position[dim_x] +
					d->rooms[room].size[dim_x] - 1));
		} while (d->item_map[p[dim_y]][p[dim_x]]);
		m->position[dim_y] = p[dim_y];
		m->position[dim_x] = p[dim_x];
		d->item_map[p[dim_y]][p[dim_x]] = m;

		d->item_map[p[dim_y]][p[dim_x]] = m;

		m->symbol = object_symbols[object.get_type()];
		m->color = object.color;
		m->isArtifact = object.artifact;
		if (m->isArtifact) {
			d->artifacts[vectorSize] = 0;
		}
		m->name = object.name;
		m->description = object.description;
		m->damage = object.damage;
		m->hit = object.hit.roll();
		m->dodge = object.dodge.roll();
		m->defence = object.defence.roll();
		m->weight = object.weight.roll();
		m->speed = object.speed.roll();
		m->attribute = object.attribute.roll();
		m->value = object.value.roll();
	}
}

	void destroy_items(dungeon *d)
	{
		memset(d->item_map, 0, sizeof(d->item_map));
	}
